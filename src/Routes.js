import { 
    createSwitchNavigator, 
    createAppContainer 
} from 'react-navigation';
import Maps from './screens/map'

const Routes = createAppContainer(
    createSwitchNavigator({
        Maps,
    })
)

export default Routes;
