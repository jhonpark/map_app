import React from 'react';
import MapViewDirection from 'react-native-maps-directions';

const Directions = ({ destination, origin, onReady }) => (
    <MapViewDirection 
        destination={destination}
        origin={origin}
        onReady={onReady}
        apikey='AIzaSyCOALSNPB2N_s9M56WK-XjSWHIR67bv670'
        strokeWidth={3}
        stokeColor="#222"
    />
);

export default Directions;
